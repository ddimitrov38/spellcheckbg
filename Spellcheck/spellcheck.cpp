//
//  spellcheck.cpp
//  Spellcheck
//
//  Created by Dimitar Dimitrov on 6/30/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <regex>
#include "stdio.h"

struct suffix {
	char character; // is the name of the character which represents this suffix
	std::string substitude; // the string of chars to strip off before adding affix
	std::string value; // the string of affix characters to add
};

struct entry {
	std::string word;
	std::vector<suffix> suffixes;
};

typedef std::map<char, std::vector<suffix>> SuffixMap;

SuffixMap suffix_map;
std::vector<entry> words;


SuffixMap loadSuffixMap(const std::string& filename);
std::vector<entry> loadWords(const std::string& filename);
std::vector<std::string> getSuffixed(entry e);
bool isCorrect(const std::string& word);
std::string autocorrectSentence(std::string s);

int main(int ac, char** av) {
	std::string line;

	suffix_map = loadSuffixMap("bg_BG.aff");
	words = loadWords("bg_BG.dic");
	printf("Loading done!\n");

	getSuffixed(words[25504]);

	printf("> ");
	while (getline(std::cin, line)) {
		autocorrectSentence(line);
//		printf("%s", autocorrectSentence(line).c_str());
		printf("> ");
	}

	return 0;
}

std::string autocorrectSentence(std::string s) {
	std::remove_if(s.begin(), s.end(), ispunct);

	std::stringstream ss(s);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> vstrings(begin, end);

	for (auto w : vstrings) {
		printf("%s%s\n", w.c_str(), isCorrect(w)?" - OK":" - NO");
	}

	return s;
}

bool isCorrect(const std::string& word) {
	for (auto e : words) {
		for (auto dict : getSuffixed(e)) {
			if (word == dict)
				return true;
		}
	}

	return false;
}

std::vector<std::string> getSuffixed(entry e) {
	std::vector<std::string> ret;

	ret.push_back(e.word);

	for (auto sfx : e.suffixes) {
		std::string newstr = e.word.substr(e.word.length() - sfx.substitude.length(), e.word.length());
		if (newstr == sfx.substitude) {
			std::string clean = e.word.substr(0, e.word.length() - sfx.substitude.length());
			ret.push_back(std::string(clean + sfx.value ));
		}
	}

	return ret;
}


SuffixMap loadSuffixMap(const std::string& filename) {
	SuffixMap suffix_map;
	FILE* f;

	if (!(f = fopen(filename.c_str(), "r"))) {
		printf("Cannot open file");
		return suffix_map;
	}

	while (!feof(f)) {
		char line[1024];
		fgets(line, 1024, f);

		suffix s;
		char sub[32];
		char rpl[32];
		char cnd[32];

		if (fscanf(f, "SFX %c   %s         %s        %s", &s.character, sub, rpl, cnd) == 4 && atoi(rpl) == 0) {
			if (strcmp(sub, "0") != 0)
				s.substitude = sub;
			else
				s.substitude = "";
			s.value = rpl;

			printf("SFX %c %s %s", s.character, s.substitude.c_str(), s.value.c_str());

			suffix_map[s.character].push_back(s);
		}
	}
	fclose(f);

	return suffix_map;
}


std::vector<entry> loadWords(const std::string& filename) {
	std::string line;
	std::vector<entry> words;
	std::regex rexp(".*/([A-Z])");
	std::smatch matches;
	std::ifstream fs(filename);

	if (!fs) {
		printf("Cannot open file");
		return words;
	}

	getline(fs, line); // Skip first line

	while (!fs.eof()) {
		getline(fs, line);

		if (line.length() < 1)
			continue;

		line.erase(line.end() - 1); // Remove the new line

		entry e; // Create the new entry
		e.word = line;


		if (std::regex_match(e.word, matches, rexp)) {
			e.suffixes = suffix_map.at(matches[1].str()[0]);
			e.word.erase(e.word.length() - 2); // Remove the suffix reference
		}

		words.push_back(e);
	}

	fs.close();

	return words;
}
